package beans;

import java.io.Serializable;
	
	public class Comment implements Serializable {
	    private static final long serialVersionUID = 1L;

	    private int id;
	    private int postid;
	    private String text;
	    public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public int getPostid() {
			return postid;
		}
		public void setPostid(int postid) {
			this.postid = postid;
		}
		public String getText() {
			return text;
		}
		public void setText(String text) {
			this.text = text;
		}
		
	}
