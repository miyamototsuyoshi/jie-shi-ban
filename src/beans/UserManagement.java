package beans;

import java.io.Serializable;
import java.util.Date;

public class UserManagement implements Serializable {
	    private static final long serialVersionUID = 1L;

	    private int id;
	    private String loginid;
	    private String name;
	    private String branch_name;
	    private int branch;
	    private String password;
	    private String position_name;
	    private int position;
	    private int stop;
	    private Date created_date;
		public Date getCreated_date() {
			return created_date;
		}
		public void setCreated_date(Date created_date) {
			this.created_date = created_date;
		}
		public int getId() {
			return id;
}
		public String getLoginid() {
			return loginid;
		}
		public void setLoginid(String loginid) {
			this.loginid = loginid;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getBranch_name() {
			return branch_name;
		}
		public void setBranch_name(String branch_name) {
			this.branch_name = branch_name;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public int getPosition() {
			return position;
		}
		public int getBranch() {
			return branch;
		}
		public void setBranch(int branch) {
			this.branch = branch;
		}
		public String getPosition_name() {
			return position_name;
		}
		public void setPosition_name(String position_name) {
			this.position_name = position_name;
		}
		public void setPosition(int position) {
			this.position = position;
		}
		public int getStop() {
			return stop;
		}
		public void setStop(int stop) {
			this.stop = stop;
		}
		public void setId(int id) {
			this.id = id;
		}
	}
