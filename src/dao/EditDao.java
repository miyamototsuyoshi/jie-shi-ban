package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserManagement;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class EditDao {
	public UserManagement getUserManagement(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT users.id,users.name,users.loginid,users.position,");
			sql.append("users.branch,branchs.branch_name,positions.position_name,users.stop from");
			sql.append(" users join branchs");
			sql.append(" on ");
			sql.append(" branchs.id = users.branch join positions");
			sql.append(" on ");
			sql.append("positions.id = users.position WHERE users.id =" + id);
			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			UserManagement ret = toUserManagementeList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private UserManagement toUserManagementeList(ResultSet rs)
			throws SQLException {
		UserManagement user = new UserManagement();

		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String loginid = rs.getString("loginid");
				int branch = rs.getInt("branch");
				String branch_name = rs.getString("branch_name");
				int position = rs.getInt("position");
				String position_name = rs.getString("position_name");
				int stop = rs.getInt("stop");

				user.setId(id);
				user.setName(name);
				user.setLoginid(loginid);
				user.setBranch(branch);
				user.setBranch_name(branch_name);
				user.setPosition(position);
				user.setPosition_name(position_name);
				user.setStop(stop);

			}

		} finally {
			close(rs);
		}
		return user;

	}

	public void update(Connection connection, UserManagement editUser) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			sql.append("UPDATE users SET");
			sql.append("  loginid = ?");
			sql.append(", name = ?");
			sql.append(", branch = ?");
			sql.append(", password = ?");
			sql.append(", position = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, editUser.getLoginid());
			ps.setString(2, editUser.getName());
			ps.setInt(3, editUser.getBranch());
			ps.setString(4, editUser.getPassword());
			ps.setInt(5, editUser.getPosition());
			ps.setInt(6, editUser.getId());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void updatePassUser(Connection connection, UserManagement editUser) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			sql.append("UPDATE users SET");
			sql.append("  loginid = ?");
			sql.append(", name = ?");
			sql.append(", branch = ?");
			sql.append(", position = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, editUser.getLoginid());
			ps.setString(2, editUser.getName());
			ps.setInt(3, editUser.getBranch());
			ps.setInt(4, editUser.getPosition());
			ps.setInt(5, editUser.getId());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	public UserManagement check(Connection connection, String loginid) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE loginid = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginid);

			ResultSet rs = ps.executeQuery();
			List<UserManagement> userList = toCheckUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserManagement> toCheckUserList(ResultSet rs) throws SQLException {

		List<UserManagement> ret = new ArrayList<UserManagement>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginid = rs.getString("loginid");

				UserManagement user = new UserManagement();
				user.setId(id);
				user.setLoginid(loginid);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}
