package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import beans.Comment;
import exception.SQLRuntimeException;

public class CommentDao {

	public void insert(Connection connection, Comment comment) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments ( ");
			sql.append("postid");
			sql.append(",text ");
			sql.append(") VALUES (");
			sql.append("?"); // loginid
			sql.append(", ?"); // password

			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, comment.getPostid());
			ps.setString(2, comment.getText());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}