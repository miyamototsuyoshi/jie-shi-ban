package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import beans.UserManagement;
import exception.SQLRuntimeException;

public class AuthorityDao {
	public UserManagement getUserManagement(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("select users.id,users.stop from users where users.id =" + id);
			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			UserManagement user = toUserManagementeList(rs);

			return user;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private UserManagement toUserManagementeList(ResultSet rs)
			throws SQLException {

		UserManagement user = new UserManagement();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				int stop = rs.getInt("stop");
				if (stop != 0) {
					stop = 0;
				} else {
					stop = 1;
				}
				user.setId(id);
				user.setStop(stop);

			}
			return user;
		} finally {
			close(rs);
		}

	}

	public void update(Connection connection, UserManagement stop) {

		PreparedStatement ps = null;
		try {

			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append("  stop = ? ");
			sql.append(" WHERE");
			sql.append(" id = ? ");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, stop.getStop());
			ps.setInt(2, stop.getId());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}
