package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;

public class UserDao {

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("loginid");
			sql.append(", password");
			sql.append(", name");
			sql.append(", branch");
			sql.append(", position");
			sql.append(", stop");
			sql.append(") VALUES (");
			sql.append("?"); // loginid
			sql.append(", ?"); // password
			sql.append(", ?"); // name
			sql.append(", ?"); // branch
			sql.append(", ?"); // position
			sql.append(", 0");//stop
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLoginid());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranch());
			ps.setInt(5, user.getPosition());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public User getUser(Connection connection, String loginid,
			String password) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE (loginid = ?) AND password = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginid);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginid = rs.getString("loginid");
				String name = rs.getString("name");
				int branch = rs.getInt("branch");
				String password = rs.getString("password");
				int position = rs.getInt("position");

				User user = new User();
				user.setId(id);
				user.setLoginid(loginid);
				user.setName(name);
				user.setBranch(branch);
				user.setPassword(password);
				user.setPosition(position);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	private List<User> toCheckUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginid = rs.getString("loginid");

				User user = new User();
				user.setId(id);
				user.setLoginid(loginid);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
	public User getUser(Connection connection, int id) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public void update(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append("  loginid = ?");
			sql.append(", name = ?");
			sql.append(", branch = ?");
			sql.append(", password = ?");
			sql.append(", position = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLoginid());
			ps.setString(2, user.getName());
			ps.setInt(3, user.getBranch());
			ps.setString(4, user.getPassword());
			ps.setInt(5, user.getPosition());
			ps.setInt(6, user.getId());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	public User check(Connection connection, String loginid) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE loginid = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, loginid);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toCheckUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {	
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	}
