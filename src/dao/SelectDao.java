package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserManagement;
import exception.SQLRuntimeException;

public class SelectDao {
	public List<UserManagement> getBranchList(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT * from branchs");
			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<UserManagement> ret = toUserManagementeList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<UserManagement> getPositionList(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append(" SELECT * from positions");
			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<UserManagement> ret = toPositionList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserManagement> toUserManagementeList(ResultSet rs)
			throws SQLException {
		List<UserManagement> ret = new ArrayList<UserManagement>();

		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String branch_name = rs.getString("branch_name");

				UserManagement user = new UserManagement();

				user.setId(id);
				user.setBranch_name(branch_name);

				ret.add(user);
			}

		} finally {
			close(rs);
		}
		return ret;

	}

	private List<UserManagement> toPositionList(ResultSet rs)
			throws SQLException {
		List<UserManagement> ret = new ArrayList<UserManagement>();

		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String position_name = rs.getString("position_name");

				UserManagement user = new UserManagement();

				user.setId(id);
				user.setPosition_name(position_name);

				ret.add(user);
			}

		} finally {
			close(rs);
		}
		return ret;

	}
}
