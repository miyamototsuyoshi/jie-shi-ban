package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserManagement;
import exception.SQLRuntimeException;

public class ManagementDao {

	public List<UserManagement> getUserManagement(Connection connection, int num) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT users.id,users.name,users.loginid,branchs.branch_name,");
			sql.append("positions.position_name,users.stop from");
			sql.append(" users join branchs");
			sql.append(" on ");
			sql.append(" branchs.id = users.branch join positions");
			sql.append(" on ");
			sql.append("positions.id = users.position ORDER BY users.id");
			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserManagement> ret = toUserManagementeList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserManagement> toUserManagementeList(ResultSet rs)
			throws SQLException {

		List<UserManagement> ret = new ArrayList<UserManagement>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String loginid = rs.getString("loginid");
				String branch_name = rs.getString("branch_name");
				String position_name = rs.getString("position_name");
				int stop = rs.getInt("stop");

				UserManagement user = new UserManagement();
				user.setId(id);
				user.setName(name);
				user.setLoginid(loginid);
				user.setBranch_name(branch_name);
				user.setPosition_name(position_name);
				user.setStop(stop);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}
