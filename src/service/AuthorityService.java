package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.UserManagement;
import dao.AuthorityDao;

public class AuthorityService {

	public UserManagement Authority(int user) {

		Connection connection = null;
		try {
			connection = getConnection();

			AuthorityDao authorityDao = new AuthorityDao();
			UserManagement stop = authorityDao.getUserManagement(connection, user);
			commit(connection);

			return stop;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void update(UserManagement stop) {

		Connection connection = null;
		try {
			connection = getConnection();

			AuthorityDao authorityDao = new AuthorityDao();
			authorityDao.update(connection, stop);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
