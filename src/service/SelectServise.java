package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.UserManagement;
import dao.SelectDao;

public class SelectServise {
	public List<UserManagement> selectBranch() {

		Connection connection = null;
		try {

			connection = getConnection();

			SelectDao selectDao = new SelectDao();
			List<UserManagement> branch = selectDao.getBranchList(connection);

			commit(connection);

			return branch;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserManagement> selectPosition() {

		Connection connection = null;
		try {

			connection = getConnection();

			SelectDao selectDao = new SelectDao();
			List<UserManagement> position = selectDao.getPositionList(connection);
			commit(connection);

			return position;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
