package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import beans.UserManagement;
import dao.EditDao;
import utils.CipherUtil;

public class EditService {
	public UserManagement edit(int userid) {

		Connection connection = null;
		try {
			connection = getConnection();

			EditDao editDao = new EditDao();
			UserManagement id = editDao.getUserManagement(connection, userid);
			commit(connection);

			return id;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void update(UserManagement editUser) {

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(editUser.getPassword());
			editUser.setPassword(encPassword);

			EditDao editDao = new EditDao();
			editDao.update(connection, editUser);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void updatePassUser(UserManagement editUser) {
		Connection connection = null;
		try {
			connection = getConnection();

			EditDao editDao = new EditDao();
			editDao.updatePassUser(connection, editUser);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}
	public UserManagement check(String loginid) {

		Connection connection = null;
		try {
			connection = getConnection();
			EditDao editDao = new EditDao();
			UserManagement user =editDao.check(connection, loginid);

			commit(connection);
			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}
}
