package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.UserManagement;
import dao.EditDao;
import dao.ManagementDao;
import utils.CipherUtil;

public class ManagementService {

	public void register(UserManagement usermanagement) {

		Connection connection = null;
		try {
			connection = getConnection();

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	private static final int LIMIT_NUM = 1000;

	public List<UserManagement> getUserManagement() {

		Connection connection = null;
		try {
			connection = getConnection();

			ManagementDao managementDao = new ManagementDao();
			List<UserManagement> ret = managementDao.getUserManagement(connection, LIMIT_NUM);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<UserManagement> getUser(int userId) {

		Connection connection = null;
		try {
			connection = getConnection();

			ManagementDao managementDao = new ManagementDao();
			List<UserManagement> user = managementDao.getUserManagement(connection, userId);

			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void update(UserManagement editUser) {

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(editUser.getPassword());
			editUser.setPassword(encPassword);

			EditDao editDao = new EditDao();
			editDao.update(connection, editUser);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}