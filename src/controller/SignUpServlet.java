package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import beans.UserManagement;
import service.SelectServise;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		SelectServise ss = new SelectServise();
		List<UserManagement> selectBranch = ss.selectBranch();
		List<UserManagement> selectPosition = ss.selectPosition();
		request.setAttribute("selectbranch", selectBranch);
		request.setAttribute("selectposition", selectPosition);
		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();
		if (isValid(request, messages) == true) {

			User user = new User();
			user.setName(request.getParameter("name"));
			user.setLoginid(request.getParameter("loginid"));
			user.setPassword(request.getParameter("password"));
			user.setPasswordCon(request.getParameter("passwordCon"));
			user.setPosition(Integer.parseInt(request.getParameter("position")));
			user.setBranch(Integer.parseInt(request.getParameter("branch")));

			new UserService().register(user);

			response.sendRedirect("./");
		} else {
			UserManagement editUser = getEditUser(request);
			session.setAttribute("errorMessages", messages);
			SelectServise ss = new SelectServise();
			List<UserManagement> selectBranch = ss.selectBranch();
			List<UserManagement> selectPosition = ss.selectPosition();
			request.setAttribute("selectbranch", selectBranch);
			request.setAttribute("selectposition", selectPosition);
			request.setAttribute("users", editUser);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	private UserManagement getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		UserManagement editUser = new UserManagement();

		editUser.setLoginid(request.getParameter("loginid"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setName(request.getParameter("name"));
		editUser.setBranch(Integer.parseInt(request.getParameter("branch")));
		editUser.setPosition(Integer.parseInt(request.getParameter("position")));

		return editUser;
	}
	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String loginid = request.getParameter("loginid");
		String password = request.getParameter("password");
		String passwordCon = request.getParameter("passwordCon");
		String name = request.getParameter("name");
		User user = new UserService().check(loginid);

		if (user != null) {
			messages.add("このユーザーIDは既に登録されています。");
		}
		if (!password.equals(passwordCon)) {
			messages.add("パスワードに相違があります。");
		}

		if (StringUtils.isBlank(loginid) == true) {
			messages.add("ログインIDを入力してください");
		} else if (!loginid.matches("azAZ0*9") && (!loginid.matches("^.{6,20}$"))) {
			messages.add("ログインIDは英数字の6-20桁のみ入力可能です");
		}
		if (StringUtils.isBlank(password) == true) {
			messages.add("パスワードを入力してください");
		} else if (!password.matches("^[\\p{Alnum}|\\p{Punct}]*$") && (!password.matches("^.{6,20}$"))) {
			messages.add("パスワードは記号を含む全ての半角文字で6-20桁のみ入力可能です");
		}
		if (StringUtils.isBlank(name) == true) {
			messages.add("名前を入力してください");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}