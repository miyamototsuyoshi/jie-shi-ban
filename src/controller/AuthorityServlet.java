package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.UserManagement;
import service.AuthorityService;

@WebServlet(urlPatterns = { "/authority" })
public class AuthorityServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		AuthorityService as = new AuthorityService();
		UserManagement stop = as.Authority(Integer.parseInt(request.getParameter("userid")));

		new AuthorityService().update(stop);
		request.getRequestDispatcher("management").forward(request, response);
	}
}