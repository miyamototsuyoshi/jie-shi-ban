package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.UserManagement;
import exception.NoRowsUpdatedRuntimeException;
import service.EditService;
import service.SelectServise;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		EditService es = new EditService();
		SelectServise ss = new SelectServise();
		UserManagement users = es.edit(Integer.parseInt(request.getParameter("userid")));
		List<UserManagement> selectBranch = ss.selectBranch();
		List<UserManagement> selectPosition = ss.selectPosition();
		request.setAttribute("selectbranch", selectBranch);
		request.setAttribute("selectposition", selectPosition);
		request.setAttribute("users", users);
		request.getRequestDispatcher("settings.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		UserManagement editUser = getEditUser(request);

		if (isValid(request, messages) == true) {

			try {
				String password = request.getParameter("password");
				if (StringUtils.isEmpty(password) != true) {
					new EditService().update(editUser);
				} else {
					new EditService().updatePassUser(editUser);
				}
			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("settings.jsp").forward(request, response);
				return;
			}

			response.sendRedirect("management");
		} else {
			session.setAttribute("errorMessages", messages);
			SelectServise ss = new SelectServise();
			List<UserManagement> selectBranch = ss.selectBranch();
			List<UserManagement> selectPosition = ss.selectPosition();
			request.setAttribute("selectbranch", selectBranch);
			request.setAttribute("selectposition", selectPosition);
			request.setAttribute("users", editUser);
			request.getRequestDispatcher("settings.jsp").forward(request, response);
		}
	}

	private UserManagement getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		UserManagement editUser = new UserManagement();

		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setLoginid(request.getParameter("loginid"));
		editUser.setPassword(request.getParameter("password"));
		editUser.setName(request.getParameter("name"));
		editUser.setBranch(Integer.parseInt(request.getParameter("branch")));
		editUser.setPosition(Integer.parseInt(request.getParameter("position")));

		return editUser;
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		int id = (Integer.parseInt(request.getParameter("id")));
		String name = request.getParameter("name");
		String loginid = request.getParameter("loginid");
		String password = request.getParameter("password");
		String passwordCon = request.getParameter("passwordCon");
		UserManagement user = new EditService().check(loginid);

		if (user != null) {
			if (user.getId() != id) {
				messages.add("このユーザーIDは既に登録されています。");
			}
		}
		if (StringUtils.isBlank(name) == true) {
			messages.add("アカウント名を入力してください");
		}
		if (StringUtils.isBlank(loginid) == true) {
			messages.add("ログインIDを入力してください");
		} else if (!loginid.matches("azAZ0*9") && (!loginid.matches("^.{6,20}$"))) {
			messages.add("ログインIDは英数字の6-20桁のみ入力可能です");
		}
		if (!password.equals(passwordCon)) {
			messages.add("パスワードに相違があります");
		} else if (!password.matches("^[\\p{Alnum}|\\p{Punct}]*$") && (!password.matches("^.{6,20}$"))) {
			messages.add("パスワードは記号を含む全ての半角文字で6-20桁のみ入力可能です");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}