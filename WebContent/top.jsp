<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">
		<div class="header">
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
			</c:if>
			<c:if test="${ not empty loginUser }">


				<a href="posts">新規投稿</a>
				<a href="./">ホーム</a>
				<a href="management">ユーザー管理</a>
				<a href="logout">ログアウト</a>
				<br />


			</c:if>
		</div>
		<c:if test="${ not empty loginUser }">
			<form action="top.jsp" method="post">
				<p>
				<div class="checkbox-inline">
					<label for="all"><input type="radio" id="all" name="search"
						value="1" checked="checked">全て</label> <label for="thisweek"><input
						type="radio" id="thisweek" name="search" value="2">今週</label> <label
						for="lastweek"><input type="radio" id="lastweek"
						name="search" value="3">先週</label>
				</div>
				<p>
					<input name="search" id="search" /> <input type="submit"
						value="検索" />
				</p>
			</form>
			<br />
			<div class="messages">
				<c:forEach items="${messages}" var="message">

					<div class="message">
						<form action="newcomments" method="post">
							<input name="id" value="${message.id}" id="id" type="hidden" />
							<div class="name">
								<br /> <br /> 投稿者 : <span class="name"><c:out
										value="${message.name}" /></span>
								<div class="date">
									<fmt:formatDate value="${message.created_date}"
										pattern="yyyy/MM/dd HH:mm" />
								</div>
							</div>
							<div class="text">
								タイトル :
								<c:out value="${message.title}" />
								<br /> <br /> カテゴリ :
								<c:out value="${message.category}" />
								<br /> <br /> 本文 :
								<c:out value="${message.text}" />
								<br />
							</div>


							<input name="comment" id="comment" /> <input type="submit"
								value="コメントする" />
						</form>
						<br />
					</div>
				</c:forEach>
			</div>
			<div class="profile">
				<div class="name">
					<h2>
						<c:out value="${loginUser.name}" />
					</h2>
				</div>
				<div class="account">
					@
					<c:out value="${loginUser.loginid}" />
				</div>
			</div>
		</c:if>

		<div class="copyright">Copyright(c)YourName</div>
	</div>
</body>
</html>
