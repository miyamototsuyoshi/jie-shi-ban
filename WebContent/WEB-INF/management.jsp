<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理</title>
<link href="css/style.css" rel="stylesheet" type="text/css">

</head>
<body>
<table>
  <tr>
    <th>ID</th>
    <th>名前</th>
    <th>ログインID</th>
    <th>所属</th>
    <th>役職</th>
    <th>作成日時</th>

  </tr>
<c:forEach var="users" items="${users}">
  <tr>
    <td>${users.id }</td>
    <td>${users.name}</td>
    <td>${users.branch }</td>
    <td>${users.position }</td>
    <td>${users.createdDate }</td>
  </tr>
</c:forEach>

</table>
<br/>



</body>
</html>