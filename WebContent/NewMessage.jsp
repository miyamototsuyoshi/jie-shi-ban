<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板</title>
 <link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">
		<div class="header">新規投稿画面</div>

		<br />
		<div class="form-area">
				<form action="posts" method="post">
					<label for="title">タイトル</label> <input name="title" id="title" /><br />
					<label for="category">カテゴリー</label> <input name="category"
						id="category" /><br /> 本文<br />
					<textarea name="message" cols="100" rows="5" class="text"></textarea>
					<br /> <input type="submit" value="投稿する">(1000文字まで）
				</form>
		</div>
		<br /> <a href="index.jsp">ホーム</a>
		<div class="profile">
			<div class="name">
				<h2>
					<c:out value="${loginUser.name}" />
				</h2>
			</div>
			<div class="account">
				@
				<c:out value="${loginUser.loginid}" />
			</div>
		</div>

		<div class="copyright">Copyright(c)YourName</div>
	</div>
</body>
</html>