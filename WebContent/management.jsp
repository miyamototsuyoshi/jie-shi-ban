<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理</title>
<link href="css/style.css" rel="stylesheet" type="text/css">

</head>
<body>
<a href="signup">ユーザー登録</a>
	<table border="1">
		<tr bgcolor="#afeeee">
			<th>ID</th>
			<th>名前</th>
			<th>ログインID</th>
			<th>所属</th>
			<th>役職</th>
			<th>編集</th>
			<th>停止・復活</th>

		</tr>
		<c:forEach var="users" items="${user}">
			<tr>
				<td>${users.getId()}</td>
				<td>${users.getName()}</td>
				<td>${users.getLoginid()}</td>
				<td>${users.getBranch_name()}</td>
				<td>${users.getPosition_name()}</td>
				<td><form action="settings" method=get>
						<input type="hidden" name="userid" value="${users.getId()}">
						<input type="submit" value="編集" />
					</form></td>
				<td><c:if test="${users.getStop()==0}">
						<form action="authority" method=get>
							<input type="hidden" name="userid" value="${users.getId()}">
							<input type="submit" value="停止"
								onclick='return confirm("アカウントを停止させます")' />
						</form>
					</c:if> <c:if test="${users.getStop()==1}">
						<form action="authority" method=get>
							<input type="hidden" name="userid" value="${users.getId()}">
							<input type="submit" value="復活"
								onclick='return confirm("アカウントを復活させます")' />
						</form>
					</c:if></td>
			</tr>
		</c:forEach>

	</table>
	<br /><br /> <a
				href="./">戻る</a>



</body>
</html>