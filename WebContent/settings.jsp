<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${users.name}の設定</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="settings" method="post">
			<br /> <input name="id" value="${users.id}" id="id" type=hidden />
			<label for="name">名前</label> <input name="name" value="${users.name}"
				id="name" /> <br /> <label for="loginid">ログインID</label> <input
				name="loginid" value="${users.loginid}" id="loginid"> <label
				for="password">パスワード</label> <input name="password" type="password"
				id="password" /> <br /> <label for="passwordCon">パスワードの確認</label>
			<input name="passwordy" type="password" id="passwordCon" /> <br />
			<label for="branch">支店</label> <select name="branch">
				<c:forEach items="${selectbranch}" var="branch">
					<c:choose>
						<c:when test="${users.branch == branch.id}">
							<option value="${branch.id}" selected>${branch.branch_name}</option>
						</c:when>
						<c:otherwise>
							<option value="${branch.id}">${branch.branch_name}</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select> <br /> <br /> <label for="position">部署・役職</label> <select
				name="position">

				<c:forEach items="${selectposition}" var="positions">
					<c:choose>
						<c:when test="${users.position == positions.id}">
							<option value="${positions.id}" selected>${positions.position_name}</option>
						</c:when>
						<c:otherwise>
							<option value="${positions.id}">${positions.position_name}</option>
						</c:otherwise>
					</c:choose>
				</c:forEach>
			</select> <br /> <br /> <input type="submit" value="登録" /> <br /> <a
				href="management">戻る</a>
		</form>

		<div class="copyright">Copyright(c)Your Name</div>
	</div>
</body>
</html>